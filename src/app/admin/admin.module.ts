import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

// import { TranslateModule } from '@ngx-translate/core';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { TopnavComponent } from './components/topnav/topnav.component';
import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
// import { NavComponent } from './nav/nav.component';

@NgModule({
    imports: [
        CommonModule,
        AdminRoutingModule,
        // TranslateModule
    ],
    // declarations: [HermesComponent, NavComponent, TopnavComponent, SidebarComponent]
    declarations: [AdminComponent, TopnavComponent, SidebarComponent]
})
export class AdminModule {}
