import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NotaRoutingModule } from './nota-routing.module';
import { NotaComponent } from './nota.component';

import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        NotaRoutingModule,
        FormsModule,
        HttpModule
    ],
    declarations: [NotaComponent]
})
export class NotaModule {}
