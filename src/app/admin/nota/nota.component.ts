import { Component, OnInit } from '@angular/core';
import { AdminService } from '../../_services/admin.service';
import { Nota } from '../../_models/Nota';
import { AppSettings } from '../../app.settings';

declare var $: any;

@Component({
  selector: 'app-nota',
  templateUrl: './nota.component.html',
  styleUrls: ['./nota.component.css']
})
export class NotaComponent implements OnInit {
  public nota: Nota;
  public notas: Nota[] = [];
  private baseUrl: string;

  constructor(private service: AdminService) {
    this.nota = new Nota();
  }

  ngOnInit() {
    /*
    // OBTENER LISTADO DE SERVICIO WEB
    this.service.findAllNotas().subscribe(data => {
      this.notas = data;
    });
    */

    /*
    $('.btn-add').click(function() {
      alert('adicionar.....');
    });
    */

    $(function() {
      $('#example1').DataTable({
        responsive: true,
        language: AppSettings.LANG_ES,
        ajax: {
          url: AppSettings.BASE_URL + '/nota/listar',
          processing: true,
          dataSrc: ''
        },
        columns: [
          { 'data': 'id' },
          { 'data': 'nombre' },
          { 'data': 'titulo' },
          { 'data': 'contenido' },
          { 'data': 'created' },
          { 'data': 'updated' },
          {
            data: null,
            className: 'center',
            defaultContent: '<a href="" class="editor_edit">Editar</a> / <a href="" class="editor_remove">Eliminar</a>'
          }
        ]
      });
    });
  }

  onGuardar() {
    // Mostrar datos del formulario por consola
    this.service.saveNota(this.nota).subscribe(data => {
      console.log(data);
    }, error => console.log('There was an error: '));
    this.nota = new Nota();
  }

}
