import { Component, OnInit } from '@angular/core';

import { KardexService } from '../../../_services/kardex.service';
import { MedEspe } from '../../../_models/MedEspe';
import { Especialidad } from '../../../_models/Especialidad';
import { Medico } from '../../../_models/Medico';

import swal from 'sweetalert';

declare var $: any;

@Component({
  selector: 'app-especialidad',
  templateUrl: './especialidad.component.html',
  styleUrls: ['./especialidad.component.css']
})
export class EspecialidadComponent implements OnInit {

  public medico: Medico;
  public medicoEspecialidad: MedEspe;
  public medicoEspecialidades: MedEspe[];
  public especialidades: Especialidad[];

  public txtFechaObtencionEspecialidad: string;

  constructor(private service: KardexService) {
    this.clearForm();
  }

  ngOnInit() {
    this.medico = new Medico();
    const idMedico = Number(localStorage.getItem('idMedico'));
    this.service.findMedicoByIdMedico(idMedico).subscribe((data: Medico) => {
      this.medico = data;
      this.onActualizarMedicoEspecialidades();
    }, error => {
      console.log('Existe un error: ' + <any>error.message);
    });
    // Mask plugin
    $('[data-mask]').inputmask('dd/mm/yyyy', {
      'placeholder': 'dd/mm/aaaa'
    });
  }

  clearForm() {
    this.txtFechaObtencionEspecialidad = '';
    this.medicoEspecialidad = new MedEspe();
    this.onActualizarEspecialidades();
  }


  onActualizarMedicoEspecialidades() {
    console.log('BUSCAR ESPECIALIDADES A EDITAR');
    this.service.findMedicoEspecialidadByIdMedico(this.medico.idMedico).subscribe(data => {
      this.medicoEspecialidades = data;
      console.log(this.medicoEspecialidades);
    });
  }

  onGuardarEspecialidad() {
    if ($('#txtFechaObtencionEspecialidad').val() !== '') {
      this.medicoEspecialidad.fechaObtencionEspecialidad = this.toDate($('#txtFechaObtencionEspecialidad').val());
    }
    this.medicoEspecialidad.idMedico = this.medico.idMedico;
    if (this.medicoEspecialidad.idMedEspe == null) {
      this.medicoEspecialidad.usuarioRegistro = 1;
      this.service.saveMedicoEspecialidad(this.medicoEspecialidad).subscribe(data => {
        if ((<any>data).code === 1) {
          // swal('Especialidad asignada!', (<any>data).message.toString(), 'success');
          swal('Especialidad asignada!', 'Se adiciono exitosamente una especialidad al médico', 'success');
          $('.mdl-especialidad').modal('hide');
          this.onActualizarMedicoEspecialidades();
        }
      }, error => {
        console.log('Existe un error: ' + <any>error.message);
      });
    } else {
      this.medicoEspecialidad.usuarioModificacion = 1;
      this.service.updateMedicoEspecialidad(this.medicoEspecialidad).subscribe(data => {
        if ((<any>data).code === 1) {
          // swal('Especialidad asignada!', (<any>data).message.toString(), 'success');
          swal('Especialidad modificada!', 'Se modifico exitosamente la especialidad del médico', 'success');
          $('.mdl-especialidad').modal('hide');
          this.onActualizarMedicoEspecialidades();
        }
      }, error => {
        console.log('Existe un error: ' + <any>error.message);
      });
    }
  }

  onEditarEspecialidad(idMedEspe: number) {
    this.service.findMedicoEspecialidadById(idMedEspe).subscribe((data: MedEspe) => {
      this.medicoEspecialidad = data;
      this.medicoEspecialidad.fechaObtencionEspecialidad = new Date(this.medicoEspecialidad.fechaObtencionEspecialidad);
      this.txtFechaObtencionEspecialidad = this.medicoEspecialidad.fechaObtencionEspecialidad.getDate() +
        '/' + (this.medicoEspecialidad.fechaObtencionEspecialidad.getMonth() + 1) +
        '/' + this.medicoEspecialidad.fechaObtencionEspecialidad.getFullYear();
    }, error => {
      console.log('Existe un error: ' + <any>error.message);
    });
  }

  onEliminarEspecialidad(medesp: MedEspe) {
    console.log(medesp);
    if (confirm('Esta seguro de desasignar la especialidad ' + medesp.idMedEspe)) {
      // console.log('Eliminando especialidad');
      this.service.deleteMedicoEspecialidaById(medesp.idMedEspe).subscribe((data) => {
        console.log(data);
        if ((<any>data).code === 1) {
          // swal('Especialidad eliminada!', (<any>data).message, 'success');
          swal('Especialidad desasignada!', 'Se ha desasignado exitosamente la especialidad', 'success');
          $('.mdl-medico').modal('hide');
          this.onActualizarMedicoEspecialidades();
        }
      }, error => {
        console.log('Existe un error: ' + <any>error.message);
      });
    }
  }

  onAdicionarEspecialidad() {
    console.log('Adicionando especialidad....');
    this.clearForm();
  }

  onActualizarEspecialidades() {
     this.service.findAllEspecialidades().subscribe(data => {
      this.especialidades = data;
    });
  }

  toDate(dateStr: string) {
    const parts: any[] = dateStr.split('/');
    return new Date(parts[2], parts[1] - 1, parts[0]);
  }

}
