import { Component, OnInit, Input } from '@angular/core';
import { KardexService } from '../../../_services/kardex.service';
import { PerfilPersonal } from '../../../_models/PerfilPersonal';
import { Hijo } from '../../../_models/Hijo';

import swal from 'sweetalert';

declare var $: any;

@Component({
  selector: 'app-datoscomplementarios',
  templateUrl: './datoscomplementarios.component.html',
  styleUrls: ['./datoscomplementarios.component.css']
})
export class DatosComplementariosComponent implements OnInit {

  public perfilPersonal: PerfilPersonal;
  public hijos: Hijo[];
  public hijo: Hijo;
  public gruposSociales: string[];
  public grupoSocial: string;
  public amigos: string[];
  public amigo: string;
  public gustosGenerales: string[];
  public gustoGeneral: string;
  public hobbies: string[];
  public hobbie: string;

  constructor(private service: KardexService) {
    this.clearForm();
  }

  ngOnInit() {
  }

  onGuardarPerfilPersonal() {
    // this.perfilPersonal.idMedico = Number(localStorage.getItem('idMedico'));
    this.perfilPersonal.nombreHijosEdad = JSON.stringify(this.hijos);
    this.perfilPersonal.gruposSociales = JSON.stringify(this.gruposSociales);
    this.perfilPersonal.amigos = JSON.stringify(this.amigos);
    this.perfilPersonal.gustosGenerales = JSON.stringify(this.gustosGenerales);
    this.perfilPersonal.hobbies = JSON.stringify(this.hobbies);
    this.perfilPersonal.usuarioRegistro = 1;
    if (this.perfilPersonal.idPerfilPersonal) {
      console.log('update');
      console.log(this.perfilPersonal.idPerfilPersonal);
      this.service.updatePerfilPersonal(this.perfilPersonal).subscribe(data => {
        console.log(data);
        swal('Datos complementarios actualizados!', (<any>data).message, 'success');
        $('.mdl-otros-datos').modal('hide');
      }, error => console.log('There was an error: ' + error));
    } else {
      console.log('create');
      console.log(this.perfilPersonal.idPerfilPersonal);
      this.service.savePerfilPersonal(this.perfilPersonal).subscribe(data => {
        console.log(data);
        swal('Datos complementarios actualizados!', (<any>data).message, 'success');
        $('.mdl-otros-datos').modal('hide');
      }, error => console.log('There was an error: ' + error));
    }
    console.log(this.perfilPersonal);
  }

  clearForm() {
    this.perfilPersonal = new PerfilPersonal();
    this.hijo = new Hijo();
    this.hijos = [];
    this.gruposSociales = [];
    this.grupoSocial = '';
    this.amigos = [];
    this.amigo = '';
    this.gustosGenerales = [];
    this.gustoGeneral = '';
    this.hobbies = [];
    this.hobbie = '';
  }

  onAdicionarHijo() {
    if (!this.hijo.nombres) {
      alert('Por favor ingrese un nombre');
    } else if (isNaN(this.hijo.edad) || this.hijo.edad < 0 || this.hijo.edad > 100) {
      alert('Por favor ingrese una edad valida');
    } else {
      this.hijos.push(this.hijo);
      this.hijo = new Hijo();
    }
  }

  onEliminarHijo(hijo: Hijo) {
    if (confirm('¿Esta seguro de eliminar el item seleccionado?')) {
      const index = this.hijos.indexOf(hijo);
      this.hijos.splice(index, 1);
    }
  }

  onAdicionarGrupoSocial() {
    if (!this.grupoSocial) {
      alert('Por favor ingrese nombre del grupo social');
    } else {
      this.gruposSociales.push(this.grupoSocial);
      this.grupoSocial = '';
    }
  }

  onEliminarGrupoSocial(grupo: string) {
    if (confirm('¿Esta seguro de eliminar ' + grupo + '?')) {
      const index = this.gruposSociales.indexOf(grupo);
      this.gruposSociales.splice(index, 1);
    }
  }

  onAdicionarAmigo() {
    if (!this.amigo) {
      alert('Por favor ingrese nombre de un amigo');
    } else {
      this.amigos.push(this.amigo);
      this.amigo = '';
    }
  }

  onEliminarAmigo(amigo: string) {
    if (confirm('¿Esta seguro de eliminar ' + amigo + '?')) {
      const index = this.amigos.indexOf(amigo);
      this.amigos.splice(index, 1);
    }
  }

  onAdicionarGustoGeneral() {
    if (!this.gustoGeneral) {
      alert('Por favor ingrese gusto general');
    } else {
      this.gustosGenerales.push(this.gustoGeneral);
      this.gustoGeneral = '';
    }
  }

  onEliminarGustoGeneral(gustoGeneral: string) {
    if (confirm('¿Esta seguro de eliminar ' + gustoGeneral + '?')) {
      const index = this.gustosGenerales.indexOf(gustoGeneral);
      this.gustosGenerales.splice(index, 1);
    }
  }

  onAdicionarHobbie() {
    if (!this.hobbie) {
      alert('Por favor ingrese un hobbie');
    } else {
      this.hobbies.push(this.hobbie);
      this.hobbie = '';
    }
  }

  onEliminarHobbie(hobbie: string) {
    if (confirm('¿Esta seguro de eliminar ' + hobbie + '?')) {
      const index = this.hobbies.indexOf(hobbie);
      this.hobbies.splice(index, 1);
    }
  }

  onEditarPersonal(idMedico: number) {
    this.service.findPerfilPersonalByIdMedico(idMedico).subscribe((data: PerfilPersonal) => {
      this.perfilPersonal = data[0];
      if (this.perfilPersonal == null) {
        this.clearForm();
        this.perfilPersonal.idMedico = idMedico;
      } else {
        this.hijos = JSON.parse(this.perfilPersonal.nombreHijosEdad);
        this.gruposSociales = JSON.parse(this.perfilPersonal.gruposSociales);
        this.amigos = JSON.parse(this.perfilPersonal.amigos);
        this.gustosGenerales = JSON.parse(this.perfilPersonal.gustosGenerales);
        this.hobbies = JSON.parse(this.perfilPersonal.hobbies);
      }
    }, error => {
      console.log('Existe un error: ' + <any>error.message);
    });
  }
}
