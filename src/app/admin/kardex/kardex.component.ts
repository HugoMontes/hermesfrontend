import { Component, OnInit, ViewChild } from '@angular/core';
import { KardexService } from '../../_services/kardex.service';
import { Medico } from '../../_models/Medico';
import { MedicoComponent } from './medico/medico.component';
import { DatosComplementariosComponent } from './datoscomplementarios/datoscomplementarios.component';

declare var $: any;

@Component({
  selector: 'app-kardex',
  templateUrl: './kardex.component.html',
  styleUrls: ['./kardex.component.css']
})
export class KardexComponent implements OnInit {

  public medico: Medico;
  public medicos: Medico[];
  // begin datatable
  public rowsOnPage = 10;
  public sortBy = 'idMedico';
  public sortOrder = 'desc';
  // end datatable

  @ViewChild(MedicoComponent) medicoComponent: MedicoComponent;
  @ViewChild(DatosComplementariosComponent) complementarioComponent: DatosComplementariosComponent;

  constructor(private service: KardexService) {
    this.medico = new Medico();
  }

  ngOnInit() {
    this.onActualizarListado();
  }

  onActualizarListado() {
    this.service.findAllMedicos().subscribe(data => {
      this.medicos = data;
    });
  }

  onAltaDatosMedico() {
    this.medicoComponent.clearForm();
  }

  onEditarDatosMedico(medico: Medico) {
    this.medicoComponent.clearForm();
    this.medicoComponent.onEditarMedico(medico.idMedico);
  }

  onEditarEspecialidad(medico: Medico) {
    localStorage.removeItem('idMedico');
    localStorage.setItem('idMedico', medico.idMedico.toString());
  }

  onEditarDatosComplementarios(medico: Medico) {
    this.complementarioComponent.clearForm();
    this.complementarioComponent.onEditarPersonal(medico.idMedico);
  }

  onEliminarKardex(medico: Medico) {
    const nombreCompleto = medico.apPaterno + ' ' + medico.nombres;
    if (confirm('Esta seguro de eliminar el kardex medico con id #' + medico.idMedico + ' de ' + nombreCompleto)) {
      this.service.deleteMedico(medico.idMedico).subscribe((data) => {
        console.log(data);
        if ((<any>data).code === 1) {
          swal('Kardex medico eliminado!', (<any>data).message, 'success');
          $('.mdl-medico').modal('hide');
          this.onActualizarListado();
        }
      }, error => {
        console.log('Existe un error: ' + <any>error.message);
      });
    }
  }
}


