import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LugartrabajoComponent } from './lugartrabajo.component';

describe('LugartrabajoComponent', () => {
  let component: LugartrabajoComponent;
  let fixture: ComponentFixture<LugartrabajoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LugartrabajoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LugartrabajoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
