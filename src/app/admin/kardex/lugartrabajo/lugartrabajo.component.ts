import L from 'leaflet';
import { Component, OnInit } from '@angular/core';

import { KardexService } from '../../../_services/kardex.service';

import { LugarTrabajo } from '../../../_models/LugarTrabajo';
import { Direccion } from '../../../_models/Direccion';
import { HorarioTrabajo } from '../../../_models/HorarioTrabajo';
import { HorarioTurno } from '../../../_models/HorarioTurno';

import swal from 'sweetalert';

// @ts-ignore
import icon from 'leaflet/dist/images/marker-icon.png';
// @ts-ignore
import iconShadow from 'leaflet/dist/images/marker-shadow.png';

declare var $: any;

const DefaultIcon = L.icon({
  iconUrl: icon,
  shadowUrl: iconShadow
});
L.Marker.prototype.options.icon = DefaultIcon;

@Component({
  selector: 'app-lugartrabajo',
  templateUrl: './lugartrabajo.component.html',
  styleUrls: ['./lugartrabajo.component.css']
})
export class LugartrabajoComponent implements OnInit {

  public lugar: LugarTrabajo;
  public lugares: LugarTrabajo[];
  public direccion: Direccion;

  public map: any;

  public horarioTrabajo: HorarioTrabajo;
  public horariosTrabajo: HorarioTrabajo[];
  public horarioTurno: HorarioTurno;

  constructor(private service: KardexService) {
    this.clearForm();
   }

  ngOnInit() {
    this.map = L.map('map').setView([-17.501319, -65.066962], 6);
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '© OpenStreetMap contributors'
    }).addTo(this.map);
    const myMarker = L.marker([-17.501319, -65.066962], { title: 'MyPoint', alt: 'The Big I', draggable: true })
      .addTo(this.map).on('dragend', function () {
        const coord = String(myMarker.getLatLng()).split(',');
        console.log(coord);
        const lat = coord[0].split('(');
        console.log(lat);
        const lng = coord[1].split(')');
        console.log(lng);
        myMarker.bindPopup('Moved to: ' + lat[1] + ', ' + lng[0] + '.');
      });
    // Timepicker
    $('.timepicker').timepicker({
      showInputs: false,
      setDate: new Date()
    });
  }

  clearForm() {
    this.lugar = new LugarTrabajo();
    this.direccion = new Direccion();
    this.horariosTrabajo = this.service.getHorariosTrabajo();
    this.horarioTrabajo = this.horariosTrabajo[0];
    this.horarioTurno = new HorarioTurno();
    this.horarioTurno.horaAtencionInicio = '06:30 PM';
    this.horarioTurno.horaAtencionFin = '06:30 PM';
  }

  onAdicionarHorario() {
    this.horarioTurno.horaAtencionInicio = $('#horaAtencionInicio').val();
    this.horarioTurno.horaAtencionFin = $('#horaAtencionFin').val();
    this.horarioTrabajo.horarioTurno.push(this.horarioTurno);
    this.horarioTurno = new HorarioTurno();
    this.horarioTurno.horaAtencionInicio = '06:30 PM';
    this.horarioTurno.horaAtencionFin = '06:30 PM';
    // console.log(this.horarioTrabajo);
  }

  onEliminarHora(hora: HorarioTurno, horario: HorarioTrabajo) {
    if (confirm('¿Esta seguro de eliminar el item seleccionado?')) {
      const index1 = this.horariosTrabajo.indexOf(horario);
      const index2 = this.horariosTrabajo[index1].horarioTurno.indexOf(hora);
      this.horariosTrabajo[index1].horarioTurno.splice(index2, 1);
    }
  }

  onGuardarLugarTrabajo() {
    swal('Kardex medico creado!', 'Lugar de trabajo guardado exitosamente', 'success');
    /*
    this.medico.fechaNacimiento = new Date($('#fechaNacimiento').val());
    this.medico.usuarioRegistro = 1;
    console.log(this.medico);
    this.service.saveMedico(this.medico).subscribe(data => {
      console.log(data);
      if ((<any>data).code === 1) {
        this.service.setIdMedico((<any>data).params.idMedico);
        swal('Kardex medico creado!', (<any>data).message, 'success');
      }
    }, error => {
      console.log('Existe un error: ' + <any>error.message);
    });
    */
  }

  onMapReady() {
    setTimeout(() => {
      this.map.invalidateSize();
    }, 1000);
  }

  saludo(){
    console.log('xxxx');
  }
}
