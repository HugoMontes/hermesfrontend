import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { KardexRoutingModule } from './kardex-routing.module';
import { KardexComponent } from './kardex.component';
import { DataTableModule } from 'angular-6-datatable';

import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { MedicoComponent } from './medico/medico.component';
import { DatosComplementariosComponent } from './datoscomplementarios/datoscomplementarios.component';
import { PrescritosComponent } from './prescritos/prescritos.component';
import { LugartrabajoComponent } from './lugartrabajo/lugartrabajo.component';
import { EspecialidadComponent } from './especialidad/especialidad.component';

@NgModule({
    imports: [
        CommonModule,
        KardexRoutingModule,
        DataTableModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule
    ],
    declarations: [
        KardexComponent,
        MedicoComponent,
        DatosComplementariosComponent,
        PrescritosComponent,
        LugartrabajoComponent,
        EspecialidadComponent]
})
export class KardexModule {}
