import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrescritosComponent } from './prescritos.component';

describe('PrescritosComponent', () => {
  let component: PrescritosComponent;
  let fixture: ComponentFixture<PrescritosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrescritosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrescritosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
