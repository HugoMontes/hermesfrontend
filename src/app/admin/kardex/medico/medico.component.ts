// import L from 'leaflet';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { KardexService } from '../../../_services/kardex.service';
import { Medico } from '../../../_models/Medico';
import { Especialidad } from '../../../_models/Especialidad';

import swal from 'sweetalert';

declare var $: any;


@Component({
  selector: 'app-medico',
  templateUrl: './medico.component.html',
  styleUrls: ['./medico.component.css']
})
export class MedicoComponent implements OnInit {

  public medico: Medico;
  public especialidades: Especialidad[];
  public txtFechaNacimiento: string;

  public swEspecialidades: boolean;

  constructor(private service: KardexService) {
    this.clearForm();
  }

  ngOnInit() {
    /*
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
    */
    // Initialize Select2 Elements
    $('.select2').select2();
    // Mask plugin
    $('[data-mask]').inputmask('dd/mm/yyyy', {
      'placeholder': 'dd/mm/aaaa'
    });
  }

  clearForm() {
    this.swEspecialidades = true;
    this.medico = new Medico();
    this.txtFechaNacimiento = '';
    this.especialidades = [];
    this.medico.genero = 1;
    this.medico.idEstadoCivil = 1;
    // Listar Especialidades
    this.service.findAllEspecialidades().subscribe(data => {
      this.especialidades = data;
    });
  }

  onGuardarMedico() {
    if ($('#txtFechaNacimiento').val() !== '') {
      this.medico.fechaNacimiento = this.toDate($('#txtFechaNacimiento').val());
    }
    console.log('idMedico: ' + this.medico.idMedico);
    if (this.medico.idMedico == null) {
      console.log('ADICIONANDO....');
      this.medico.usuarioRegistro = 1;
      console.log(this.medico);
      this.service.saveMedico(this.medico).subscribe(data => {
        if ((<any>data).code === 1) {
          swal('Kardex medico creado!', (<any>data).message, 'success');
          $('.mdl-medico').modal('hide');
          this.actualizarListadoMedico();
        }
      }, error => {
        console.log('Existe un error: ' + <any>error.message);
      });
    } else {
      console.log('ACTUALIZANDO....');
      this.medico.usuarioModificacion = 1;
      console.log(this.medico);
      this.service.updateMedico(this.medico).subscribe(data => {
        if ((<any>data).code === 1) {
          swal('Kardex medico actualizado!', (<any>data).message, 'success');
          $('.mdl-medico').modal('hide');
          this.actualizarListadoMedico();
        }
      }, error => {
        console.log('Existe un error: ' + <any>error.message);
      });
    }
  }

  onEditarMedico(idMedico: number) {
    this.swEspecialidades = false;
    console.log('EDITAR MEDICO..........');
    this.service.findMedicoByIdMedico(idMedico).subscribe((data: Medico) => {
      this.medico = data;
      this.medico.fechaNacimiento = new Date(this.medico.fechaNacimiento);
      this.txtFechaNacimiento = this.medico.fechaNacimiento.getDate() +
        '/' + (this.medico.fechaNacimiento.getMonth() + 1) +
        '/' + this.medico.fechaNacimiento.getFullYear();
      this.medico.usuarioRegistro = 1;
      // BUSCAR ESPECIALIDADES DEL MEDICO
      /*
      this.service.findMedicoEspecialidadByIdMedico(this.medico.idMedico).subscribe(espmed => {
        this.medico.lstEspecialidad = espmed;
      });
      console.log('MEDICO ENCONTRADO....');
      console.log(this.medico);
      */
    }, error => {
      console.log('Existe un error: ' + <any>error.message);
    });
  }

  // tslint:disable-next-line:member-ordering
  @Output() updateEvent = new EventEmitter();
  actualizarListadoMedico() {
    this.updateEvent.next();
  }

  toDate(dateStr: string) {
    const parts: any[] = dateStr.split('/');
    return new Date(parts[2], parts[1] - 1, parts[0]);
  }
}
