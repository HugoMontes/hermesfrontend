import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { KardexComponent } from './kardex.component';
import { LugartrabajoComponent } from './lugartrabajo/lugartrabajo.component';
import { EspecialidadComponent } from './especialidad/especialidad.component';

const routes: Routes = [
    {
        path: '',
        component: KardexComponent
    },
    {
        path: 'lugartrabajo',
        component: LugartrabajoComponent
    },
    {
        path: 'especialidad',
        component: EspecialidadComponent
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class KardexRoutingModule { }
