import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
// import { LoginComponent } from './login/login.component';
// import { KardexComponent } from './admin/kardex/kardex.component';
// import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
// import { AdminComponent } from './admin/admin.component';

import { AuthenticationService } from './_services/authentication.service';
import { AdminService } from './_services/admin.service';
import { KardexService } from './_services/kardex.service';

import { AppRoutingModule } from './app-routing.module';

/************** ROUTES *******************/
/*
const routes: Routes = [
  {  path: '', component: LoginComponent },
  {  path: 'login', component: LoginComponent },
  {  path: 'kardex', component: KardexComponent },
  {  path: 'nota', component: NotaComponent },
  {path:'', component: AreaLoginComponent},
  {path:'admin', component: AdminComponent},
  {path:'admin/comprasptiposproveedor', component: COMPRASPTIPOSPROVEEDORComponent},
  {path:'admin/compraspcargosextra', component: COMPRASPCARGOSEXTRAComponent},
  {path:'admin/compraspcategoriasproveedor', component: COMPRASPCATEGORIASPROVEEDORComponent},
  {path:'admin/compraspmediospago', component: COMPRASPMEDIOSPAGOComponent},
  {path:'admin/comprasptiposcompra', component: COMPRASPTIPOSCOMPRAComponent},
  {path:'admin/comprasptiposmercaderiacompras', component: COMPRASPTIPOSMERCADERIACOMPRASComponent},
  {path:'admin/comprasptipospagos', component: COMPRASPTIPOSPAGOComponent},
  {path:'admin/comprasptipostransporte', component: COMPRASPTIPOSTRANSPORTEComponent},
];
*/
@NgModule({
  declarations: [
    AppComponent,
//    LoginComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
//    RouterModule.forRoot(routes),
//    FormsModule,
//    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [
    AuthenticationService,
    AdminService,
    KardexService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
