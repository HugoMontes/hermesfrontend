import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { AppSettings } from '../app.settings';

@Injectable()
export class AuthenticationService {

    private baseUrl: string;

    constructor(private http: HttpClient) {
        this.baseUrl = AppSettings.BASE_URL;
    }

    login(username: string, password: string) {
        const options = { headers: new HttpHeaders().set('Content-Type', 'application/json') };
        const body = JSON.stringify({ nombreUsuario: username, contrasenia: password });
        return this.http.post<any>(this.baseUrl + '/login', body, options)
            .pipe(map(usuario => {
                // login successful if there's a jwt token in the response
                if (usuario && usuario.token) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('usuario', JSON.stringify(usuario));
                }
                return usuario;
            }));
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
    }
}
