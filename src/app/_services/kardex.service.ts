import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { AppSettings } from '../app.settings';

import { Medico } from '../_models/Medico';
import { Especialidad } from '../_models/Especialidad';
import { PerfilPersonal } from '../_models/PerfilPersonal';
import { HorarioTrabajo } from '../_models/HorarioTrabajo';
import { MedEspe } from '../_models/MedEspe';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class KardexService {

    private baseUrl: string;

    constructor(private http: HttpClient) {
        this.baseUrl = AppSettings.BASE_URL + 'kardex/';
    }

    // ********* SERVICIOS MEDICO ************//
    findAllMedicos() {
        return this.http.get<Medico[]>(this.baseUrl + 'medico/listar');
    }

    findMedicoByIdMedico(id: number) {
        const json = '{"idMedico": ' + id + '}';
        return this.http.post(this.baseUrl + 'medico/obtener', JSON.parse(json), httpOptions);
    }

    saveMedico(medico: Medico) {
        return this.http.post(this.baseUrl + 'medico/adicionar', medico, httpOptions);
    }

    updateMedico(medico: Medico) {
        return this.http.post(this.baseUrl + 'medico/actualizar', medico, httpOptions);
    }

    deleteMedico(id: number) {
        const json = '{"idMedico": ' + id + '}';
        return this.http.post(this.baseUrl + 'medico/eliminar', JSON.parse(json), httpOptions);
    }
    // ********* /SERVICIOS MEDICO ************//

    // ********* SERVICIOS MED_ESPE ************//
    findMedicoEspecialidadById(id: number) {
        const json = '{"idMedEspe": ' + id + '}';
        return this.http.post(this.baseUrl + 'medespe/obtener', JSON.parse(json), httpOptions);
    }
    findMedicoEspecialidadByIdMedico(id: number) {
        const json = '{"idMedico": ' + id + '}';
        return this.http.post<MedEspe[]>(this.baseUrl + 'medespe/listar', JSON.parse(json), httpOptions);
    }
    saveMedicoEspecialidad(medespe: MedEspe) {
        return this.http.post(this.baseUrl + 'medespe/adicionar', medespe, httpOptions);
    }
    updateMedicoEspecialidad(medespe: MedEspe) {
        return this.http.post(this.baseUrl + 'medespe/actualizar', medespe, httpOptions);
    }
    deleteMedicoEspecialidaById(id: number) {
        const json = '{"idMedEspe": ' + id + '}';
        return this.http.post<MedEspe[]>(this.baseUrl + 'medespe/eliminar', JSON.parse(json), httpOptions);
    }
    // ********* /SERVICIOS MED_ESPE ************//

    // ********* SERVICIOS ESPECIALIDAD ************//
    findAllEspecialidades() {
        return this.http.get<Especialidad[]>(this.baseUrl + 'especialidad/listardescripcion');
    }
    // ********* /SERVICIOS ESPECIALIDAD ************//

    // ********* SERVICIOS PERFIL_PERSONAL ************//
    savePerfilPersonal(perfilPersonal: PerfilPersonal) {
        return this.http.post(this.baseUrl + 'perfilpersonal/adicionar', perfilPersonal, httpOptions);
    }

    updatePerfilPersonal(perfilPersonal: PerfilPersonal) {
        return this.http.post(this.baseUrl + 'perfilpersonal/actualizar', perfilPersonal, httpOptions);
    }

    findPerfilPersonalByIdMedico(id: number) {
        const json = '{"idMedico": ' + id + '}';
        return this.http.post(this.baseUrl + 'perfilpersonal/listar', JSON.parse(json), httpOptions);
    }

    findByIdPerfilPersonal(id: number) {
        const json = '{"idPerfilPersonal": ' + id + '}';
        return this.http.post(this.baseUrl + 'perfilpersonal/obtener', JSON.parse(json), httpOptions);
    }
    // ********* /SERVICIOS PERFIL_PERSONAL ************//

    getHorariosTrabajo() {
        const dias = ['Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes'];
        const horariosTrabajo: HorarioTrabajo[] = [];
        for (const dia of dias) {
            const horario = new HorarioTrabajo();
            horario.dia = dia;
            horario.horarioTurno = [];
            horariosTrabajo.push(horario);
          }
        return horariosTrabajo;
    }
}
