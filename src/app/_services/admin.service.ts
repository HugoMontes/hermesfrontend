import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Nota } from '../_models/Nota';
import { Medico } from '../_models/Medico';
import { AppSettings } from '../app.settings';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class AdminService {

    private baseUrl: string;

    constructor(private http: HttpClient) {
        this.baseUrl = AppSettings.BASE_URL;
    }

    findAllNotas() {
        return this.http.get<Nota[]>(this.baseUrl + '/nota/listar');
    }

    saveNota(nota: Nota) {
        return this.http.post(this.baseUrl + '/nota/adicionar', nota, httpOptions);
    }

}
