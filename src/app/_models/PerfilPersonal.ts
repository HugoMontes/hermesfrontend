export class PerfilPersonal {
    idPerfilPersonal: number;
    idMedico: number;
    nombreConyugue: string;
    nombreHijosEdad: string;
    origenFamilia: string;
    gruposSociales: string;
    amigos: string;
    comidaPreferida: string;
    bebidaPreferida: string;
    mascota: string;
    tipoMusicaPreferida: string;
    instrumentoPreferido: string;
    gustosGenerales: string;
    hobbies: string;
    usuarioRegistro: number;
}
