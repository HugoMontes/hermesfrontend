export class MedEspe {
    idMedEspe: number;
    idMedico: number;
    idEspecialidad: number;
    idCategoria: number;
    fechaObtencionEspecialidad: Date;
    lugarObtencionEspecialidad: string;
    usuarioRegistro: number;
    usuarioModificacion: number;
    estado: number;
}
