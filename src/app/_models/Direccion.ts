export class Direccion {
    idDireccion: number;
    zona: string;
    avenidaCalle: string;
    nroDireccion: string;
    telefono: string;
    email: string;
    avenida_calle: string;
    usuarioRegistro: number;
}
