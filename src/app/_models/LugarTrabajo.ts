export class LugarTrabajo {
    idMedico: string;
    descripcion: string;
    nroPacientes: number;
    costoConsulta: number;
    longitud: string;
    latitud: string;
    cargo: string;
    usuarioRegistro: number;
}
