export class Nota {
    id: number;
    nombre: string;
    titulo: string;
    contenido: string;
    created: string;
    updated: string;
}
