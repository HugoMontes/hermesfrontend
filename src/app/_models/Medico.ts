import { Especialidad } from './Especialidad';
// import { MedEspe } from './MedEspe';
export class Medico {
    idMedico: number;
    apPaterno: string;
    apMaterno: string;
    nombres: string;
    fechaNacimiento: Date;
    genero: number;
    idEstadoCivil: number;
    telefono: string;
    celular: string;
    correoElectronico: string;
    usuarioRegistro: number;
    usuarioModificacion: number;
    lstEspecialidad: Especialidad[];
    // lstEspecialidad: MedEspe[];
}
