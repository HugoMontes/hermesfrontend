export class HorarioTurno {
    idTurno: number;
    descripcion: string;
    horaAtencionInicio: string;
    horaAtencionFin: string;
}
